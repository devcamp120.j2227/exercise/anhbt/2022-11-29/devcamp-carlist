import {gJsonCars, isNew} from "./info"
function App() {
  var cars = JSON.parse(gJsonCars)
  
  return (
    <div>
      <ul>{cars.map(function(car, index) {
        return <li key={index}>
          {car.make} {car.model} {car.vId}
          {isNew(car.year) ? "mới" : "cũ"}
        </li>
      })}</ul>
    </div>
  );
}

export default App;
